<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?=base_url('')?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url('')?>assets/libs/css/style.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="<?=base_url('')?>assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

    <title>Admin - BarberShop</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
         <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
         <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="#">
                        <small class="font-weight-bold mb-0 text-uppercase"><span style="color:#A52A2A;"><bold>BarberShop</span></small>
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item">
                            <div id="custom-search" class="top-search-bar">
                                <input class="form-control" type="text" placeholder="Search..">
                            </div>
                                <a class="dropdown-item" href="<?=base_url('auth/logout')?>"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
      <div class="nav-left-sidebar" style="background-color:#FFFFFF;">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav" style="background-color: #A0522D;" >
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <span style="color:#FFFFFF;">MENU</span>
                            </li>
                                <li class="nav-item bg-light ">
                                            <a class="nav-link" href="index.html" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1-2" aria-controls="submenu-1-2"><i class="fas fa-cut" style="color:#FFA500;"></i><span style="color:#000000;">Menu</span></a>
                                            <div id="submenu-1-2" class="collapse submenu  bg-light " style="">
                                                <ul class="nav flex-column bg-light ">
                                                    <li class="nav-item bg-light    ">
                                                        <a class="nav-link bg-light " href="<?=base_url('menu')?>"><span style="color:#A0522D;">Daftar Menu</span></a>
                                                    </li>
                                                </ul>
                                                <ul class="nav flex-column bg-light ">
                                                    <li class="nav-item bg-light    ">
                                                        <a class="nav-link bg-light " href="<?=base_url('menu/tambah')?>"><span style="color:#A0522D;">Tambah Menu</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="nav-item bg-light">
                                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#transaksi" aria-controls="transaksi"><i class="fas fa-fw fa-dollar-sign" style="color:#FFA500;"></i><span style="color:#000000;">Transaksi</span></a>
                                            <div id="transaksi" class="collapse submenu  bg-light" style="">
                                                <ul class="nav flex-column bg-light">
                                                    <li class="nav-item bg-light">
                                                        <a class="nav-link bg-light" href="<?=base_url('transaksi')?>"><span style="color:#A0522D;">Data Transaksi</span></a>
                                                    </li>
                                                </ul>
                                                <ul class="nav flex-column bg-light">
                                                    <li class="nav-item bg-light">
                                                        <a class="nav-link bg-light" href="<?=base_url('transaksi/history')?>"><span style="color:#A0522D;">History Transaksi</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- KONTEN -->
                        


                    